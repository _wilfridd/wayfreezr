# WayFreezr

Waydroid container freeze and unfreeze.

A simple tool that freeze and unfreeze Waydroid's container on Ubuntu Touch. It provides battery saving and a secure locking of Waydroid while the container is freezed.

## License

Copyright (C) 2023  wilfridd

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
