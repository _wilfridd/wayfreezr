import pyotherside
import time
import os
import subprocess
import threading
import sys
sys.path.append('deps')
sys.path.append('../deps')
import pexpect
import password_type

class ContainerOperation:
    def get_password_type(self):
        return password_type.get_password_type()

    def checkState(self):
        os.chdir("/home/phablet")
#        print("Checking Waydroid state")
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        waydroidstatus = subprocess.getoutput("waydroid status | grep Cont | awk '{print $2}'")
        # time.sleep(0.5)
        return waydroidstatus

    def checkOperation(self):
        os.chdir("/home/phablet")
#        print("Checking Waydroid state")
        pyotherside.send('whatOperState',"> Checking Waydroid state... ")
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        waydroidstatus = subprocess.getoutput("waydroid status | grep Cont | awk '{print $2}'")
        # time.sleep(0.5)
        pyotherside.send('whatWayStatus',"...State checked!")
        return waydroidstatus

    def freeze(self, password, shellHistory):
        os.chdir("/home/phablet")
#        print("shellHistory : ", shellHistory)

        #Starting bash and getting root privileges
#        print("Starting bash and getting root privileges")
        pyotherside.send('whatOperState',"> Privilege operation...")
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        if shellHistory == '0':
#            print("Freeze sudo no hist")
            child.sendline(' sudo -s')
        else:
#            print("Freeze sudo hist")
            child.sendline('sudo -s')
        time.sleep(0.5)
        if password != '':
            child.expect('[p/P]ass.*')
            child.sendline(str(password))
        child.expect('root.*')

        #Freezing
#        print("Freezing Waydroid")
        pyotherside.send('whatOperState',">> Freezing Waydroid...")
        if shellHistory == '0':
#            print("Container freeze no hist")
            child.sendline(' waydroid container freeze')
        else:
#            print("Container freeze hist")
            child.sendline('waydroid container freeze')
        time.sleep(0.75)
        child.expect('root.*')
        pyotherside.send('whatWayStatus',"...Freeze complete!")

    def unfreeze(self, password, shellHistory):
        os.chdir("/home/phablet")
#        print("shellHistory : ", shellHistory)

        #Starting bash and getting root privileges
#        print("Starting bash and getting root privileges")
        pyotherside.send('whatOperState',"> Privilege operation...")
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        if shellHistory == '0':
#            print("Unfreeze sudo no hist")
            child.sendline(' sudo -s')
        else:
#            print("Unfreeze sudo hist")
            child.sendline('sudo -s')
        time.sleep(0.5)
        if password != '':
            child.expect('[p/P]ass.*')
            child.sendline(str(password))
        child.expect('root.*')

        #Unfreezing
#        print("Unfreezing Waydroid")
        pyotherside.send('whatOperState',">> Unfreezing Waydroid...")
        if shellHistory == '0':
#            print("Container unfreeze no hist")
            child.sendline(' waydroid container unfreeze')
        else:
#            print("Container unfreeze hist")
            child.sendline('waydroid container unfreeze')
        time.sleep(0.75)
        child.expect('root.*')
        pyotherside.send('whatWayStatus',"...Unfreeze complete!")

    def wayShowFullUi(self, shellHistory):
        os.chdir("/home/phablet")
#        print("shellHistory : ", shellHistory)

        #Starting bash and unfreezing Waydroid
#        print("Starting bash")
        child = pexpect.spawn('bash')
        child.expect(r'\$')

        #Unfreezing
#        print("Passwordless Unfreezing Waydroid")
        pyotherside.send('whatOperState',">> Unfreezing Waydroid passwordless...")
        if shellHistory == '0':
#            print("Unfreeze passwordless no hist")
            child.sendline(' waydroid show-full-ui')
        else:
#            print("Unfreeze passwordless hist")
            child.sendline('waydroid show-full-ui')
        time.sleep(0.75)
        pyotherside.send('whatWayStatus',"...Unfreeze complete!")

containeroperation = ContainerOperation()
