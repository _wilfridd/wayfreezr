/*
 * Copyright (C) 2023  wilfridd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wayfreezr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'wayfreezr.wilfridd'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property var inputMethodHints: Qt.ImhHiddenText
    property var isPasswordNumeric: inputMethodHints & Qt.ImhDigitsOnly
    property var shellHistory: !startupSettings.shellHistory ? "0" : "1"
    property var pwdlessUnfreeze: !startupSettings.pwdlessUnfreeze ? "0" : "1"
    property var refreshOnfocus: !startupSettings.refreshOnfocus ? "0" : "1"

    // Remember settings
    Settings {
        id: startupSettings
        category: "StartupSettings"
        property bool forceDarkmode: false
        property bool shellHistory: true
        property bool pwdlessUnfreeze: false
        property bool refreshOnfocus: false
    }

    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push(wayfreez)
        }

        AboutPage {
            id: aboutpage
            visible: false
        }

        SettingsPage {
            id: settingspage
            visible: false
        }

        WayFreez {
            id: wayfreez
            visible: false
        }

    }//PageStack
    //Password type is determined at app launch
    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importModule('password_type', () => {
                const PASSWORD_TYPE_KEYBOARD = 0;
                const PASSWORD_TYPE_NUMERIC = 1;

                python.call('password_type.get_password_type', [], (passwordType) => {
                    const value = python.getattr(passwordType, 'value');

                    switch (value) {
                        case PASSWORD_TYPE_KEYBOARD:
                            root.inputMethodHints = Qt.ImhHiddenText;
                            return;
                        case PASSWORD_TYPE_NUMERIC:
                            root.inputMethodHints = Qt.ImhHiddenText | Qt.ImhDigitsOnly;
                            return;
                    }

                    root.inputMethodHints = null;
                });
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }//Python
} //main
