import QtQuick 2.9
import Lomiri.Components 1.3

Page {
    id: aboutPage

    header: PageHeader {
        title: i18n.tr("About")
    }

    ScrollView {
        id: scrollView
        anchors {
            top: aboutPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        clip: true

        Column {
            id: aboutColumn
            spacing: units.gu(2)
            width: scrollView.width

            //dirty but enable space below header
            ListItem {
              id: emptySpaceAP
              height: parent.width < parent.height ? units.gu(2.5) : units.gu(1)
              divider.visible: false
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("↶ WayFreezr ↷")
                fontSize: "large"
            }

            LomiriShape {
                width: units.gu(12); height: units.gu(12)
                anchors.horizontalCenter: parent.horizontalCenter
                radius: "medium"
                image: Image {
                    source: Qt.resolvedUrl("../assets/Wayfreezr.png")
                }
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                linkColor: LomiriColors.orange
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Version: ") + "1.5.0" + " | " + " <a href='https://gitlab.com/_wilfridd/wayfreezr'>" + i18n.tr("Source") + "</a>" // | <a href='https://gitlab.com/_wilfridd/wayfreezr/issues'>" + i18n.tr("ISSUES") + "</a>
                // + "%1".arg(Qt.application.version)
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                linkColor: LomiriColors.orange
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("A simple tool that allows to freeze and unfreeze WayDroid's container on Ubuntu Touch.")
            }

            Label {
                width: parent.width
                linkColor: LomiriColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                // Note to TRANSLATORS: Change the URL according to the language
                text: i18n.tr("This program is free software under the terms of the <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU General Public License.</a>")
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Copyright") + " (c) 2023 wilfridd. If you like this app, you are welcome to make a donation..."
            }

            Label {
                width: parent.width
                linkColor: LomiriColors.green
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                fontSize: "large"
                //style: Font.DemiBold
                text: "<a href='http://revolut.me/wilfribl77'>" + i18n.tr("DONATE") + "</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}
