import QtQuick 2.9
import Lomiri.Components 1.3

Page {
    id: settingspage

    header: PageHeader {
        id: settingsHeader
        title: i18n.tr("Settings")
    }

    function applyTheme() {
        if (forceDarkmodeSwitch.checked)
            Theme.name = 'Lomiri.Components.Themes.SuruDark'
        else Theme.name = ''
    }

    Column {
        id: settingsColumn

        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        //dirty but enable space below header
        ListItem {
          id: emptySpaceSP
          height: settingsColumn.width < settingsColumn.height ? units.gu(5) : units.gu(4.5)
          divider.visible: false
        }

        ListItem {
            id: appTheme
            height: appThemeLayout.height / 1.6
            divider.visible: false
            ListItemLayout {
                id: appThemeLayout
                title.text: "<b>" + i18n.tr("Theme") + "<\b>"
                title.font.pointSize: units.gu(1.5)
            }
        }

        ListItem {
            id: forceDarkmodeItem
            height: forceDarkmodeLayout.height
            divider.visible: true
            ListItemLayout {
                id: forceDarkmodeLayout
                title.text: i18n.tr("Force dark mode")
                title.font.pointSize: units.gu(1.4)
                Switch {
                    id: forceDarkmodeSwitch
                    SlotsLayout.position: SlotsLayout.Last
                    checked: startupSettings.forceDarkmode
                    onCheckedChanged: {
                        startupSettings.forceDarkmode = checked;
                        applyTheme();
                    }
                }
            }
            onClicked: {
                forceDarkmodeSwitch.checked = !forceDarkmodeSwitch.checked;
            }
        }

        ListItem {
            id: appBehavior
            height: appBehaviorLayout.height / 1.6
            divider.visible: false
            ListItemLayout {
                id: appBehaviorLayout
                title.text: "<b>" + i18n.tr("Behavior") + "<\b>"
                title.font.pointSize: units.gu(1.5)
            }
        }

        ListItem {
            id: refreshOnfocus
            height: refreshOnfocusLayout.height / 1.4
            divider.visible: false
            ListItemLayout {
                id: refreshOnfocusLayout
                title.text: i18n.tr("Auto-refresh on focus")
                title.font.pointSize: units.gu(1.4)
                Switch {
                    id: refreshOnfocusSwitch
                    SlotsLayout.position: SlotsLayout.Last
                    checked: startupSettings.refreshOnfocus
                    onCheckedChanged: {
                        startupSettings.refreshOnfocus = checked;
                    }
                }
            }
            onClicked: {
                refreshOnfocusSwitch.checked = !refreshOnfocusSwitch.checked;
            }
        }

        ListItem {
            id: pwdlessUnfreeze
            height: pwdlessUnfreezeLayout.height
            divider.visible: false
            ListItemLayout {
                id: pwdlessUnfreezeLayout
                title.text: i18n.tr("Passwordless unfreeze")
                title.font.pointSize: units.gu(1.4)
                Switch {
                    id: pwdlessUnfreezeSwitch
                    SlotsLayout.position: SlotsLayout.Last
                    checked: startupSettings.pwdlessUnfreeze
                    onCheckedChanged: {
                        startupSettings.pwdlessUnfreeze = checked;
                    }
                }
            }
            onClicked: {
                pwdlessUnfreezeSwitch.checked = !pwdlessUnfreezeSwitch.checked;
            }
        }

        ListItem {
            id: shellHistoryItem
            height: shellHistoryItem.height
            divider.visible: true
            Label {
                x: units.gu(2)
                width: parent.width / 1.4
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Record freezing/unfreezing cycles in shell command line history")
                font.pixelSize: units.gu(1.9)
            }
            Switch {
                id: shellHistorySwitch
                anchors.right: parent.right
                anchors.rightMargin: units.gu(2)
                SlotsLayout.position: SlotsLayout.Last
                checked: startupSettings.shellHistory
                onCheckedChanged: {
                    startupSettings.shellHistory = checked;
                }
            }
            onClicked: {
                shellHistorySwitch.checked = !shellHistorySwitch.checked;
            }
        }
    } //Column
}
