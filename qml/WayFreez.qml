import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Page {
    id: wayfreez
    header: PageHeader {
        id: wayfreezHeader
        title: i18n.tr("Waydroid container tool")
        opacity: 1
        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr("About")
                    onTriggered: {pageStack.push(aboutpage)}
                    iconName: "info"
                },
                Action {
                    text: i18n.tr("Settings")
                    onTriggered: {pageStack.push(settingspage)}
                    iconName: "settings"
                }
            ]
        }
    } //PageHeader

    property var waydroidstatus: "unChecked"
    property var waydroidstatuslbl: waydroidstatus
    property var statusflick:""

    function checkOperation() {
        python.call('containeroperation.checkOperation', [], (returnWaydroidstatus) => {
            checkState();
        });
        python.setHandler('whatOperState', (operState) => {
            lbloperation.text = operState;
        });
        python.setHandler('whatWayStatus', (wayStatus) => {
            lbloperation.text = wayStatus;
            waydroidstatuslbl = waydroidstatus;
        });
    }

    function checkState() {
        python.call('containeroperation.checkState', [], (returnWaydroidstatus) => {
            if (returnWaydroidstatus.includes('not found'))
                waydroidstatus = "NOT INSTALLED";
            if (returnWaydroidstatus.includes('FROZEN'))
                waydroidstatus = returnWaydroidstatus;
            if (returnWaydroidstatus.includes('RUNNING'))
                waydroidstatus = returnWaydroidstatus;
            if (returnWaydroidstatus === '')
                waydroidstatus = "STOPPED";
                waydroidstatuslbl = waydroidstatus;
                activity.running = false;
        });
    }

    function startOperation(password, shellHistory) {
        lbloperation.text = "";
        if (waydroidstatus === "RUNNING")
            python.call('containeroperation.freeze', [password, shellHistory]),
            checkState();
        else
            python.call('containeroperation.unfreeze', [password, shellHistory]),
            checkState();
    }

    function waydroidOperation(shellHistory) {
        activity.running = true;
        python.call('containeroperation.wayShowFullUi', [shellHistory]);
        checkState();
        destroyAnimation.start();
    }

    function delay(delayTime,cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    MainView {
        id: wayfreezView
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        ListItem {
            id: lstItemStatus
            height: wayfreezView.width < wayfreezView.height ? units.gu(10) : units.gu(4)
            divider.visible: true
            color: {
                if (waydroidstatuslbl == "FROZEN")
                    color: "#5ebbfb"
                else if (waydroidstatuslbl == "RUNNING")
                    color: "#faaf03"
                else color: "transparent"
            }
        }

        Label {
            id: lblstatus
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: lstItemStatus.verticalCenter
            width: parent.width / 1.1
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Container is : " + "<b>" + waydroidstatuslbl + "<\b>")
            font.pointSize: units.gu(2)
            wrapMode: Text.Wrap
        }

        Label {
            id: lblbutton
            anchors.top: lstItemStatus.bottom
            anchors.topMargin: wayfreezView.width < wayfreezView.height ?
                parent.height / 4.5 : parent.height / 6

            x: units.gu(2)
            width: parent.width / 2
            font.pointSize: units.gu(1.6)
            wrapMode: Text.Wrap
            text: waydroidstatus == "unChecked" ? ""
                : waydroidstatus == "NOT INSTALLED" ? i18n.tr("Install Waydroid first...")
                : waydroidstatus == "STOPPED" ? i18n.tr("Start Waydroid first...")
                : i18n.tr("Press button to operate Waydroid")
        }

        Button {
            id: operButton
            anchors.right: parent.right
            anchors.rightMargin: units.gu(2)
            anchors.verticalCenter: lblbutton.verticalCenter
            anchors.topMargin: parent.height / 12.5
            enabled: !activity.running
            visible: waydroidstatus == "STOPPED"
                || waydroidstatus == "NOT INSTALLED"
                || waydroidstatus == "unChecked" ? false : true
            color: theme.palette.normal.positive
            text: waydroidstatus == "FROZEN" ? i18n.tr("Unfreeze") : i18n.tr("Freeze")
            onClicked: {
                if (pwdlessUnfreeze == '0')
                    PopupUtils.open(passwordPrompt);
                else if (waydroidstatus == "FROZEN")
                    lbloperation.text = "",
                    createAnimation.start(),
                    waydroidOperation(shellHistory);
                else
                    PopupUtils.open(passwordPrompt);
                return;
            }
        }

        Label {
            id: lbloperation
            anchors.top: operButton.bottom
            anchors.topMargin: parent.height / 8
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: units.gu(1.6)
            width: parent.width / 1.5
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
            text: ""

            NumberAnimation on opacity {
                id: destroyAnimation
                from: 5
                to: 0
                duration: 5000
            }

            NumberAnimation on opacity {
                id: createAnimation
                from: 0
                to: 1
                duration: 100
            }
        }

        Flickable {
            id: flickArea
            anchors {
                top: lblbutton.bottom
                topMargin: parent.height / 12
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            flickableDirection: Flickable.VerticalFlick
            //Prevent flick when running -> prevent multiple refresh
            interactive: !activity.running ? true : false

            onMovementStarted: {
                createAnimationFlick.start()
                statusflick = "Pull to refresh";
            }

            onMovementEnded: {
                destroyAnimationFlick.start();
            }

            onFlickStarted: {
                activity.running = true
                checkOperation();
            }

            onFlickEnded: {
                statusflick = "Release to refresh";
            }

            Label {
                id: lblflick
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height / 2
                anchors.horizontalCenter: parent.horizontalCenter
                font.pointSize: units.gu(1.6)
                color: theme.palette.normal.positive
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
                text: statusflick

                NumberAnimation on opacity {
                    id: destroyAnimationFlick
                    from: 1
                    to: 0
                    duration: 2500
                }

                NumberAnimation on opacity {
                    id: createAnimationFlick
                    from: 0
                    to: 1
                    duration: 100
                }
            }

        } //Flickable

        ActivityIndicator {
            id: activity
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: parent.height / 16
            running: true
        }

    } //MainView

    Timer {
        id: timer
    }

    Connections {
        target: Qt.application
        onStateChanged: {
            if (refreshOnfocus == '1' && Qt.application.state == 4)
                    activity.running = true,
                    checkOperation();
            return;
        }
    }

    Component {
        id: passwordPrompt
        PasswordPrompt {
            onPassword: {
                lbloperation.text = "";
                createAnimation.start();
                activity.running = true;
                startOperation(password, shellHistory);
                destroyAnimation.start();
            }
            onCancel: {
                activity.running = false;
            }
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));
            importNames('containeroperation', ['containeroperation'], () => {
            });
            if (refreshOnfocus == '0')
                checkOperation();
            return;
            destroyAnimation.start();
        }

        onError: {
            console.log('python error:', traceback);
        }
    }
}
